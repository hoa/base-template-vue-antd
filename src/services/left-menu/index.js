export default [
  {
    key: "demo",
    icon: "appstore-o",
    path: "/demo",
    name: "Demo",
  },
  {
    key: "demo-btn",
    icon: "cloud-o",
    path: "/demo/btn",
    name: "Demo Button",
  },
  {
    key: "1",
    icon: "user",
    path: "/",
    name: "nav 1",
  },
  {
    key: "2",
    icon: "video-camera",
    path: "/",
    name: "nav 2",
  },
  {
    key: "3",
    icon: "upload",
    path: "/",
    name: "nav 3",
  },
  {
    key: "4",
    icon: "bar-chart",
    path: "/",
    name: "nav 4",
  },
  {
    key: "5",
    icon: "cloud-o",
    path: "/",
    name: "nav 5",
  },
  {
    key: "6",
    icon: "appstore-o",
    path: "/",
    name: "nav 6",
  },
  {
    key: "7",
    icon: "team",
    path: "/",
    name: "nav 7",
  },
  {
    key: "8",
    icon: "shop",
    path: "/",
    name: "nav 8",
  },
];
