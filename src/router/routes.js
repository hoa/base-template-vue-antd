import LayoutApp from "@/layouts/layout-app";

import LayoutDemo from "@/pages/demo/index";
import ListDemo from "@/pages/demo/ListDemo";
import BtnDemo from "@/pages/demo/btn/ListBtn";

export default [
  {
    path: "/",
    component: LayoutApp,
    children: [
      {
        path: "demo",
        component: LayoutDemo,
        children: [
          {
            path: "",
            component: ListDemo,
          },
          {
            path: "btn",
            component: BtnDemo,
          },
        ],
      },
    ],
  },
];
