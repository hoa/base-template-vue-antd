import Vue from "vue";

import Antd from "ant-design-vue";
import App from "./App";
import "ant-design-vue/dist/antd.css";
import "./theme.css";
import router from "./router";
Vue.use(Antd);
Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
